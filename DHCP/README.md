[TOC]

# M123 DHCP Configuration

## Auftrag 1.

## Filius Aufgabe
**Ausgangslage**: Der DHCP-Server ist nicht konfiguriert und nicht eingeschaltet.

Es war der Auftrag, den DHCP-Server zu konfigurieren. So dass der Client 1 und der Client 2 eine IP-Adresse aus dem Pool des DHCP-Servers bekommen und der Client 3 bekommt vom DHCP eine fixe IP-Adresse.

![Ausgangslage](./images/1.png) <br>
<br><br>



## Konfiguration
Ich habe den DHCP-Server und den DHCP-Dienst auf allen Clients aktiviert. Ich habe dann dem DHCP so **konfiguriert**, dass er den Clients 1 und 2 eine normale IP-Adresse aus seinem Pool vergibt.

Aber dem Client 3 (mit der MAC-Adresse 49:11:5C:DA:77:23) soll der DHCP-Server immer die IP-Adresse 192.168.0.50 zuweisen.

![konfiguriert](./images/2.png)<br>
<br><br>

## Der Test

Nachdem ich mit dem Konfigurieren fertig war, war es Zeit zu **testen**, ob die Geräte sich gegenseitig anpingen können

Bsp. (Client 1 zu Client 3)

![testen](./images/3.png)

Der versuch war erfolgreich und somit auch die Aufgabe.
<br><br><br><br>





## Auftrag 2.

### 1. Router-Konfiguration auslesen

Es galt mit den unten stehenden commands zunächst einmal die Ausgangslage zu betrachten. <br>

Zunächst sollte man in den richtigen Modus wechseln mit em command "enable". Danach hat man mit "show ip dhcp pool" herausgelesen, wie der Pool der DHCP (Router) aussah (wie viele IP's sind geleased welche excloded usw.).

 Der nächste command "show ip dhcp binding" zeigte welche IP-Adresse welcher MAC-Adresse zugeteilt ist.

 Und der letzte command "show running-config" zeigt unter anderem nochmals die beiden Range's der excluded IP-Adressen.

 Fragen:

 1. Für welches Subnetz ist der DHCP Server aktiv?
 
 Der DHCP ist für ein Subnetz aktiv (Range: bis 254)

 2. Welche IP-Adressen ist vergeben und an welche MAC-Adressen?

192.168.21.22 | 00D0.BC52.B29B<br>
192.168.21.27 | 0009.7CA6.4CE3<br>
192.168.21.24 | 0050.0F4E.1D82<br>
192.168.21.26 | 00E0.8F4E.65AA<br>
192.168.21.25 | 0001.632C.3508<br>
192.168.21.23 | 0007.ECB6.4534

3. In welchem Range vergibt der DHCP-Server IPv4 Adressen?

192.168.21.1 - 192.168.21.254 

4. Was hat die Konfiguration ip dhcp excluded-address zur Folge?

Excluded adresses sind z.B. die Netz-ID und der Broadcast 

5. Wie viele IPv4-Adressen kann der DHCP-Server dynamisch vergeben?

Alle IP-Adressen von 192.168.21.22 bis 192.168.21.176. Die IPv4 Adressen von 192.168.21.1 bis 192.168.21.21 und von 192.168.21.177 192.168.21.241 sind für Dinstleisungen reserviert.



### 2. DORA - DHCP Lease beobachten

Mit der Anleitung haben wir den DORA (Discover, Offer, Request, Ack) simuliert. Zuerst wurde ein Paket per broadcast an alle Clients gesendet, wobei der Router (DHCP Dienst) als einziger reagiert hat.

Dann wurde das Paket mit einer Offer zurück an PC-2 gesendet. Anschliessend ging der Request zurück an den Router (DHCP Dienst). Als letztes ging das Acknowlege Datenpaket wieder zurück an den Client. So läuft der ganze DORA Prozess ab.


Fragen:

1. Welcher OP-Code hat der DHCP-Offer?

Der DHCP-Offer hat den OP-Code OP:0x0000000000000002 


2. Welcher OP-Code hat der DHCP-Request?

Der DHCP-Request hat den OP-Code OP:0x0000000000000001 


3. Welcher OP-Code hat der DHCP-Acknowledge?

Der DHCP-Acknowledge hat den OP-Code OP:0x0000000000000002 


4. An welche IP-Adresse wird der DHCP-Discover geschickt? Was ist an dieser IP-Adresse speziell?

Der DHCP-Discover wird per Broadcast an alle Geräte im Netzwerk.


5. An welche MAC-Adresse wird der DHCP-Discover geschickt? Was ist an dieser MAC-Adresse speziell?

Es wird ebenfalls über die MAC-Adresse gebroadcastet (die MAC-Adresse sieht so aus: FFFF.FFFF.FFFF).


6. Weshalb wird der DHCP-Discover vom Switch auch an PC3 geschickt?

Der Switch senden den DHCP-Discover an alle per Broadcast (somit auch an PC3). 


7. 


8. Welche IPv4-Adresse wird dem Client zugewiesen?

Es ist eine der IP-Adressen in der Range 192.168.21.22 bis 192.168.21.176, diese sind für Clients vorgesehen. 



### Screenshots

DHCP-Discover

![DHCP-Discover](./images/DHCPDiscover.png)



DHCP-Offer

![DHCP-Discover](./images/DHCPOffer.png)



DHCP-Request

![DHCP-Discover](./images/DHCPRequest.png)



DHCP-Acknowladge

![DHCP-Discover](./images/DHCPACK.png)



### 3. Netzwerk umkonfigurieren

Dem Server wurde eine Statische IPv4-Adresse zugeteilt - 192.168.21.241. Der Gateway wurde ausserdem statisch auf 192.168.21.1 festgelegt.

IPv4 Server:

![Server-IPv4](./images/IPv4_Server.png)



IPv4 Server Gateway:

![Server-IPv4-Gateway](./images/IPv4_Server_Gateway.png)



Webside-Server:

![Webside-Server](./images/Webside-Server.png)


## Aufgabe: DHCP Server mit Ubuntu konfigurieren

Aufgabenstellung: Die Aufgabe war es, einen DHCP Server mit Linux Ubuntu eizurichten und zu konfigurieren. Das Ziel dabei sollte sein, dem Server eine Statische IP bei der Netztwerkkarte des Privaten Netzes zu geben und ausserdem auch eine NAT Netzwerkkarte im Server laufen zu lassen. Ausserdem sollte ein Windows 10 Client, aus dem Modul 117 eine IP-Adresse von DHCP erhalten.

Als erstes musste ich natürlich erst einmal eine virtuelle Maschine mit Linux Ubuntu aufsetzen, wobei ich mich für die Ubuntu 22.04 Version entschieden habe, die uns von Herr Calisto auf empfohlen wurde. Nach dem kurzen Einrichten meines Benutzers habe ich mit dem Befehl: "sudo apt update" wie in der gegebenen Anleitung beschrieben ein Update durchgeführt.

Anschlessend konnte das eingentliche Arbeiten mit dem DHCP-Server beginnen. Als aller erstes habe ich der VM zusätzlich zum NAT-Adapter auch noch eine Netzwerkkarte mit dem Priveten Netzwerk (M117) hinzugefügt.

![NAT/M117-Netzwerk](./images/NAT-M117.png)

Anschliessend habe ich mit dem Befehl "sudo apt install isc-dhcp-server -y" den Dienst hinzugefügt, der den Server zu einem DHCP-Server macht.

Als nachstes habe ich, nach der Anleitung, im Verzeichnit "/etc/default/isc-dhcp-server" das Interface des Servers angegeben (in meinem Fall war das Interface "enp2s5).

![Interface-Eintrag](./images/Interface-Eintrag.png)

Im nächsten Schritt, ging es an die Konfiguration des DHCP-Dienstes ansich.Dazu habe ich den Befehl "sudo nano /etc/dhcp/dhcpd.conf" ausgeführt und habe im Kanfigurationsverzeichnis folgende dinge geändert: 

Ich habe vor folgende zwei Texte ein # gesetzt, damit diese zum Kommentar werden und kein gültiger Befehl mehr sind:

#option domain-name "example.org";
#option domain-name-servers ns1.example.org, ns2.example.org;

Ausserdem, habe habe ich dem DHCP-Dienst auch einen range vorgegeben der in unserem Fall von 192.168.100.51 bis 192.168.100.150 reichte (Subnetzmaske 255.255.255.0 und routers 192.168.100.1).

![#options](./images/optionen.png)

![range](./images/range.png)

Nachdem ich die gesammte Konfiguration abgeschlossen und gespeichert hatte, war es Zeit zu testen, ob der DHCP-Server (an sich) funktioniert. Zuerst habe ich ihn mit "sudo systemctl start isc-dhcp-server" eingeschaltet, anschliessend mit "sudo systemctl enable isc-dhcp-server" aktiviert und dann mit dem letzten Befehl "sudo systemctl status isc-dhcp-server" den Status überprüft. Leider hat es nicht gut ausgesehen:

![fehler](./images/fehler.png)

Nach der Gezeigten Fehlermeldung habe ich alles versucht, den DHCP-Server zum Laufen zu bringen. Ich habe in den Konfigurationen mehrmals die Range angepasst (bzw. die Formulierung des Befehls) ausserdem habe ich das Interface mehrmals überprüft und sichergestellt, dass alle ausgeführten Befehle korrekt waren. Ich habe zudem auch mehrmals die Anleitung durgeschaut.

Nach diesem troubleshooting bin ich zum Entschluss gekommen, dass es ein Problem mit der IP-Adresse des Servers bzw. mit den Netzwerkkarten in der virtuellen Maschine geben muss. Ich habe versucht, was ebenfalls Teil des Auftrags war, der M117 Netzwerkkarte eine fixe IP-Adresse zuzuweisen. Daran bin ich allerdings gescheitert.

Ich werde mir in einer freien Minute im Betrib Zeit nehmen, den DHCP-Server so aufzusetzen, dass er funktioniert. Selbst wenn ich däfür Hilfe brauche. 

## Die Korrektur 

Mein troubleshootig war teilweise richtig. Ich musste tatsächlich die IP-Adresse des DHCP-Servers konfigurieren, was ich im folgenden Screenshot auch getan habe.

![IP_DHCP_Configuration](./images/IP_DHCP_configuration.png)

Allerdings hatte ich, abgesehen von der IP-Adresse, auch das Interface falsch angegeben (statt ens37 habe ich ens33 angegeben). Im Folgenden Screenshot habe ich  dies noch angepasst.

![interface_configuration](./images/Interface.png)

Somit war der Auftrag abgeschlossen.

