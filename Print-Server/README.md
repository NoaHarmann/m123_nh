[TOC]

## Print Server:


#### Installation:

Die Installation des Servers und des Print-Tools läuft ählich ab, wie die Installation des DNS-Tools (in der git Dokumentation "DNS" ist der Installationsprozess sehr genau bebildert). 

Statt "DNS Server" wählt man bei der Installation "Print and Document Services".


#### Netzwerk


Wichtig zu bearchten ist hier, dass die physische Maschine im Netzwerk U60 sein muss, da in diesem Netzwerk die Drucker sind. Ausserdem muss man dem Server, statt wie in den letzten Aufträgen ein NAT Netzwerkkarte und eine Netzwerkkarte mit dem privaten Netzwerk, eine gebridge Netzwerkkarte an Stelle der NAT Netzwerkkarte nehmen.

Diese Netzwerkkarte hat dan die IP 172.16.17.15 bekommen.

![Netzwerk-Printer](./images/Netzwerk-Printer.png)


#### Konfiguration: 


Wenn man im Server Manager auf "Tools" oben rechts klickt und dan "Print Management" öffnet, kommt man in die Einstellungen des Print-Dienstes.

![Print-Tool](./images/Print-Tool.png)

![Print-Management](./images/Print-Management.png)


Ins Dropdown-Menü Print Server und auch das darauffolgende öffnen (WIN-.....). Dann ein Rechtsklick auf "Printer" und auf "Add Printer" (zu deutsch: Drucker hinzufügen). Das folgende Fenster wird geöffnet:

![add-printer](./images/Add-Printer.png)

Mit der Auswahl "Search the Network for Printers" und einem Klick auf "Next" werden alle Drucker im Netzwerk angezeigt.

![print-Auswahl](./images/Print-Auswahl.png)

Die in der Aufgabe vorgegebenen Drucker sind die mit den IPv4 Adresssen 172.16.17.71 und 172.16.17.72.

"Next"

![new-driver](./images/new-Driver.png)

"Install a new driver"

"Next"

In diesem Fenster muss man den Treiber auswählen. Da der Gewänschte Treiber wahrscheinlich nicht angezeigt wird, muss man zuerst auf "Windows Updates" drücken. Nachdem das Update beendet ist: Unter Hersteller "HP" ganz runterscrollen und "HP Universal Printing PCL 6" auswählen.

"Next"

"Next"

"Next"

Ich würde empfehlen, den "Test Print" anzuwählen um zu überprüfen ob der Server richtig verbunden ist.

Hat geklappt!

#### Test/Troubleshoot auf dem Client:

Leider war es weder mir noch irgend einem anderen der Schüler gelungen etwas über den Client auszudrucken. Ich kann zwar auf den Print-Server über den Client zugreifen, wo mir der Drucker auch angezeigt wird, ich kann mich aber leider nicht mit dem Drucker verbinden.

Leider kann ich hier kein Bild der Fehlermeldung anhängen, da ich diese Dokumentation ausserhalb des Netzes im U60 geschrieben habe und daher kann ich mir die Drucker nicht mehr anzeigen lassen.