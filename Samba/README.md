# Samba File Share


[TOC]



Nach dem Aufsetzen des DHCP-Servers und des DNS-Servers kommt nun auch der Samba Dienste/Server dazu. Der Samba Dienste/Server ist eine Möglichkeit Dateien über das Netzwerk zu teilen. Es ist im Prinzip eine Form von NAS (Network Attached Storage).

In dieser Aufgabe wurde und Frei gestellt, ob wir Ubuntu Server ohne GUI oder Ubuntu Desktop mit GUI verwenden.

Ich habe mich für den Ubuntu Desktop 22.04.03 entschieden.



## Installation/Konfiguration des Tools über das Terminal 

Ich habe die Anleitung benutzt, die uns für die Aufgabe gegeben wurde. Ausserdem habe ich der Ubuntu VM natürlich noch die Netzwerkkarte für des private Netzwerk hinterlegt.

Als ich, nach dem Einrichten, auf dem Ubuntu Desktop war habe ich über die Registerkarte das Terminal geöffnet.

![Registerkarte_Ubuntu](./images/Registerkarte_Ubuntu.png)

![Registerkarte_Terminal](./images/Registerkarte_Terminal.png)


Im Terminal habe ich dann zuerst einmal ein Update gemacht, mit den command "sudo apt update".

![Update](./images/Update.png)

Danach habe ich den Samba Dienst installiert, mit dem command "sudo apt install samba". Mit dem command "systemctl status smbd --no-pager -l" konnte ich dann überprüfen, ob der Dienst läuft.

![Samba_Status](./images/Samba_Status.png)

Ich habe dann mit dem command "sudo systemctl enable --now smbd" noch konfiguriert, dass der Dienst immer mit der VM startet.

![Samba_auto_start](./images/auto_start.png)

Mit dem command "sudo ufw allow samba" habe ich dann  noch der Ubuntu VM erlaubt auf die Firewall zuzugreifen.

## Benutzer hinzufügen

Ich musste nun nur noch einen Benutzer zum Samba Dienst hinzufügen. Dies kann man mit dem command "sudo usermod -aG sambashare $USER" machen.

#### Gut zu wissen: 

Bei diesem Befehl kann man das $USER mit dem Benutzernamen erstetzen, den man zum Dienst hinzufügen will. Tut man das nicht, wird eifach der aktuelle user genommen.

Mit dem comman "sudo smbpasswd -a $USER" habe ich dem lokalen Benutzer dann noch ein Passwort für den Dienst gesetzt.

## File share

Ich habe nun über das GUI einen Ordner geteilt. Dafür bin ich links in der Taskleiste auf das Folder Symbol gegangen und habe dan auf "Persönlicher Ordner" (in englisch Home)

![Home_Verzeichnis](./images/Home_Verzeichnis.png)

Mit einem Rechtsklick habe ich dann einen Testordner erstellt und ihn "test" genannt. Dann habe ich mit folgenden Schritten den Ordner geteilt: Rechtsklick auf den Ordner und auf "Eigenschaften" (Properties auf englisch) - dann auf "Freigabe im lokalen Netzwerk" (Local Network Share auf englisch) - "Diesen Ordner freigeben". Anschliessend muss man noch die Berechtigug für den geteilten Ordner wählen, ich habe sowohl die Berechtigung zum Ändern der Dateien als auch die Gast Freigabe ausgewählt.

![Sharing](./images/Sharing.png)


## Das Testen

Um die Funktion des File Shares zu testen, habe ich eine der beiden VM's aus M117 gestertet. Zuerst habe ich dann getestet, ob die Windows VM die Ubuntu VM durch pingen erreichen kann. Ich habe in der Kommandozeile "ping 192.168.19.142" angegeben, was die IP-Adresse des NAT der Ubuntu VM ist. Der ping hat funktioniert.

![ping](./images/ping.png)

 Dann bin ich auf den File Explorer und dann mit einen Rechtsklick auf Netzwerke zu Netzlaufwerk verbinden. 

Dort habe ich dann die IP-Adresse der Ubuntu vm angegebein \\192.168.19.142 und dann habe ich noch den Ordner angegeben \test. Als ich Enter gedrückt habe, ist eine Anmeldemaske aufgetaucht. Ich musse hier die Daten eingeben, die ich beim Hinzufügen des Benutzers auf der Ubuntu VM hinterlegt hatte.

Der Ordner "test" den ich erstellt hatte ist aufgegangen. Der Test war somit erfongreich.