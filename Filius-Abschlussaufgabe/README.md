# Filius-Abschlussaufgabe


[TOC]

#### Konfiguration:

Auftrag: Am Ende sollte das Netzwerk so aufgebaut sein:

![Filius-Vorlage](./images/Filius-Vorlage.png)

Als erstes habe ich alle Geräte wie auf dem vorgegebenen Bild miteinander verbunden und habe danach die Ports am "Vermittlungsrechner" (Router) konfiguriert. 

![Router-Ports](./images/Routerports.png)

Anschliessend habe ich allen Geräten die eine Statische IP-Adresse bekommen eine zugeteil (dazu gehören sowohl die auf dem vorgegebenen Bild gezeigten PC's als auch die Server).

Hier ein Beispiel von PC11:

![PC11](./images/PC11.png)

Bei den zwei PC's im andernen Subnetz wurde die Konfiguration dem entsprechend angepasst.

Ich habe für die restlichen PC's im /8 Subnetz, die nach vorgegebenem Bild alle die ihre IP-Adresse vom DHCP bekommen, eine Range im DHCP-Server eingegeben und auch gleich die restlichen Konfigurationen eingetragen.

![DHCP-Filius](./images/Filius-DHCP.png)

Ein kleiner Test bei PC12:

![IP-PC12](./images/IP-PC12.png)

Ausserdem hat jedes Gerät, dem ich eine Statische IP-Adresse zugeteilt habe, eien Eintrag im DNS bekommen.

![DNS-Filius](./images/DNS-Filius.png)

