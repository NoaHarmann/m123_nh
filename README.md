# M123 Serverdienste in Betrieb nehmen 

### Einleitung

In dieser Gitlab Zusammenfassung sind die erfüllten Aufträge des Moduls 123. Darunter befinden sich Theoretische Aufträge, die sich mit dem Prozess des DHCP befassen. Ausserdem sind die praktischen Aufträge zu DHCP und DNS beide in diesen Verzeichnissen dokumentiert.

1. [DHCP - Linux Ubuntu](./DHCP/README.md)
2. [DNS - Windows Server 2022](./DNS/README.md)