# DNS-Server aufsetzen


[TOC]



### Die Aufgabe

Der Auftrag: Wir sollten einen DNS-Server, mit Windows Server 2022, aufsetzen der im Sota Netzwerk der letzten Aufträge funktioniert.



## Installation Windows Server 2022

Als erstes habe ich das ISO-Image vom Azure der Schule heruntergeladen. In VMware-Workstation habe ich dann eine Neue VM mit dem bevorzugten OS Windows Server 2022. Im ISO-Image selber waren 4 verschiedene Versionen des OS, einmal standart mit und ohne GUI und einmal datacenter mit und ohne GUI.

![Windows-Server-Versionen](./images/Windows_Server_Versionen.png)



## Installation des DNS Tools

###### Hinweis:
(Ich habe für die Schritte zum einrichten des DNS Servers ein Youtube Tutorial verwendet)

Sobald ich den Account für den Windows Server erstellt hatte und mich eingeloggt habe, ist der Server Manager auf dem Desktop aufgegeangen.

![Server-Manager](./images/Server%20Manager.png)


Als nächstes habe ich auf "Add roles and features" gedrückt, wobei dieses Fenster aufgeht.

![Add_roles_and_features](./images/Add_roles_features.png)


Dann habe ich auf "Next" geklickt, wobei ein Fenster aufgegangen ist dass mich nach dem Installationstyp abgefragt hat.

![Before_you_start](./images/Before%20you%20begin.png)


Ich habe "Role-based or feature-based installation" ausgewählt und erneut auf Next gedrückt.

![installation_type](./images/installation_type.png)


Auf der nächsten Seite "Sever Selection" musste ich anwählen, auf welchem Server ich Tools oder Features installieren will. Da ich nur einen (Virtuellen) Server habe, habe ich das einzige angewählt dass vorgeschlagen wurde und auf "Next" gedrückt.

![Server_Selection](./images/Server_selection.png)


Beim Klicken auf "Next" ist ein kleines Fenster aufgegangen mit einer Auswahl an Server Roles. Ich habe "DNS Server" ausgewählt und auf "Next" und dann "Install" geklickt.

![DNS_Server_Installation](./images/DNS_Tool_Installation.png)



## Netzwerkeinstellungen

Damit der DNS-Server überhaupt in unserem Netzwerk funktioniert, musste ich dem Server erst eine Netzwerkkarte mit dem Privaten Customnetzwerk M117_Lab hinzufügen (Neben der NAT Verbindung natürlich). Das Netzwerk han, nach wie vor, die Netzwerk IP 192.168.100.0 (ebenfalls zu sehen in der DHCP Dokumentation). 

![NAT_Privat_Serer](./images/NAT_Privat_Server.png)


Ich habe dann dem Privaten M117_Lab Netzwerk (bzw. der Netzwerkkarte) eine IPv4-Adresse gegebem, nähmlich 192.168.100.2. Die Subnetzmaske war standartmässig 255.255.255.0. Das Feld für den Gateway habe ich leer gelassen und als DNS-Server habe ich den Server selbst angegeben (192.168.100.2). Als alternativen DNS habe ich den Google Server angegeben 8.8.8.8.

![IP_Server](./images/IP_Server.png)


Natürlich muss ich bei dieser Aufgabe am Ende testen, ob der Client mit dem DNS Server kommunitieren kann. Aus diesem Grund musste ich natürlich auch bei meinem Test Client den DNS-Server bei der Privaten Netzwerkkarte eintragen. DNS 192.168.100.2 und alternativ der google DNS 8.8.8.8.



##### Sehr Wichtig
Weil ich die Netzwerkkarte mit dem privagen Netzwerk erst nachträglich hinzugefügt habe, musste ich auch bei der NAT Verbindung den DNS-Server angeben. Ansonsten hätte es nicht funktioniert.

Privates Netzwerk M117_Lab:

![Adapter_Client_Privat](./images/Adapter_Client_Privat.png)


Öffentliches Netzwerk NAT:

![Adapter_Client_NAT](./images/Adapter_%20Client_NAT.png)



## DNS Konfiguration

Nun war es Zeit den DNS-Server an sich zu konfigurieren. Ich habe im Server Manager rechts oben auf "Tools" gedrückt und das "DNS" Tool angeklickt.

![DNS_Server_Tool](./images/DNS_Server_Tool.png)

Ein Fenster mit dem DNS Manager ist aufgegangen.

![DNS_Manager](./images/DNS_Manager.png)



### Forward Lookup Zone

Als erstes musste ich (nach der Anleitung) eine so genannte "Forward Lookup Zone" erstellen. Eine "Forward Lookup Zone" übersetzt, einfach gesagt, einen Domain Namen in eine IP-Adresse.

Ich habe einen Rechtsklick auf "Forward Lookup Zone" gemacht und dann "New Zone" angewählt. Danach ist folgendes aufgetaucht (Ich habe mich durch das Menü durchgeklickt, wie Ihr in den folgenden Bildern seht).

![New_Zone_wizard](./images/New_Zone_Wizard_Forward_Lookup_Zone.png)

"Next >"

(Ich habe "Primary Zone" für eine Primäre Zone erstellt)

![Primary_Zone_Forward](./images/Primary_Zone_Forward.png)

"Next >"

Hier musste ich den Zonen Namen eintragen. Ich wollte die Aufgabe so originalgetreu wie möglich machen. Darum habe ich die Zone "sota.com" genannt.

![Zone_name](./images/Zone_Name.png)

"Next >"

Ich musste in diesem Fenster die Art der Zone File Erstellung bestimmen. Ich habe "Create a new file with this file name" (ich denke, dass keine ausführliche Erklärung nötig ist).

![Zone_File](./images/Zone_File.png)

"Next >"

Ich habe bei dieser Auswahl "Do not allow dynamic updates" ausgewählt da ich, besonders in einer Test VM, keine nicht manuellen Updates haben will.

![Dynamic_Updates_Forward](./images/Dynamic_Updates-Forward.png)

"Next >"

"Finish"

![Forward_Finish](./images/Forward_Finish.png)

Nun ist die "Forward Lookup Zone" erstellt worden.



### Reverse Lookup Zone

Die zweite Zone die ich erstellen musste heisst "Reverse Lookup Zone". Die "Reverse Lookup Zone" macht das Gegenteil der "Forward Lookup ZoneA", sie übersetzt die IP-Adresse zum Domain Namen.

Ich habe einen Rechtsklick auf "Reverse Lookup Zone" gemacht und dann "New Zone" angewählt. Danach ist folgendes aufgetaucht (Ich habe mich durch das Menü durchgeklickt, wie Ihr in den folgenden Bildern seht).

![New_Zone_Wizard_Reverse_Lookup_Zone](./images/New_Zone_Wizard_Reverse_Lookup_Zone.png)

"Next >"

(Ich habe "Primary Zone" für eine Primäre Zone erstellt)

![Primary_Zone_Reverse](./images/Primary_Zone_Reverse.png)

"Next >"

Ich musste nun die IP-Adressenversion wählen, für die "Übersetzung" von IP-Adresse zum Domain Namen.

![Reverse_IP_Version](./images/Reverse_IP-Version.png)

"Next >"

Auf dieser Seite musste ich bestimmen, im welcher Range (von IPv4-Adressen im Netzwerk) die "Reverse Lookup Zone" aktiv sein soll. Da ich in den vorherigen Schritten die IP-Adresse des Servers und des Clients geändert habe, habe ich hier 192.168.100 eingegeben.

![Reverse_IP-Range](./images/Reverse_IP_Range.png)

"Next >"

Ich musste dann noch auswählen, ob ich ein neues File erstellen will oder ob ich das existierende benutzen will. Da ich zum ersten mal einen DNS-Server aufgesetzt habe, habe ich auf "Create a new File with the file name" geklickt. (also ein neues File erstellt)

![Reverse_Zone_File](./images/Reverse_Zone_File.png)

"Next >"

Ich habe die "Dynamic Updates" genau wie bei der "Forward Lookup Zone" deaktiviert 

![Dynamic_Updates_Reverse](./images/Dynamic_Updates_Reverse.png)

"Next >"

"Finish"

Nun ist die "Reverse Lookup Zone" ebenfalls erstellt worden.



### Host erstellen

Nachdem ich die "Forward Lookup Zone" und die "Reverse Lookup Zone" erstellt habe, muss ich nun noch die Hosts in Form von A-records erstellen und zwar einmal für den Server und einmel für den Client (damit der Client den DNS erreichen kann und der DNS den Client).

Ich habe die erstellte Zone "sota.com" in der "Forwerd Lookup Zone" geöffnet und einen Rechtsklick ausgeführt. Dort hatte ich dan die Option, einen A-record zu erstellen indem ich auf "New Host (A or AAAA)...".

![A-Record_erstellen](./images/A-Record_erstellen.png)

Da ich als erstes den Host (A-record) für den Client erstellen wollte, habe ich in das Feld "Name" den Begriff "client" reingeschrieben. Dadurch hat sich der FQDN (also der Fully qualified domain name) der im Kästchen darunter angezeig wird, auch gleich angepasse.

Da dies der A-record für den Client werden soll, habe ich im Kästchen für die IP-Adresse die IP-Adresse des Clients angegeben (192.168.100.53). 

Wichtig!: Wenn man das Häkchen bei "Create associated pointer (PTR) record" setzt, wird der Host direkt in die erstellte "Reverse Lookup Zone" übernommen.

![Host_A-record_Client](./images/Host_A-Record_Client.png)

Sobald man auf "Add Host" klickt kommt eine Meldung, dass der Host (A-record) in der "Forward" und in der "Reverse Lookup Zona" erstellt wurde.

![Bestätigungsmeldung](./images/Bestätigungsmeldung.png)

Den Selben Vorgeng, mit dem Erstellen des Hosts (A-records), musste für den Server wiederholt werden. Also statt "client" schreibt man "server" und statt der IP-Adresse vom Client gibt man die IP-Adresse vom Server ein (in meinem Fall 192.168.100.2). Natürlich habe ich auch hier das Häkchen gesetzt, dass den Host-Eintrag von der "Forward" auch in die "Reverse Lookup Zone" übernimmt.

![Host_A-Record](./images/Host_A-Record_Server.png)



### Das Testen

Ich habe die Kommandozeile des Clients geöffnet und habe den Hefehl nslookup verwendet um die Kommunikation zwischen DNS-Server und Client zu testen.

In habe als erstes den Befehl "nslookup 192.168.100.53" eingegeben (Sozusagen die Verbindung des Clients und desswegen auch die IP-Adresse des Clients)

![nslookup_Client](./images/nslookup_192-168-100-53.png)

Als näschstes habe ich das Gleiche gemacht aber mit der IP-Adresse des Severs: 192.168.100.2 (Sozusagen die Verbindung des Servers)

![nslookup_Server](./images/nslookup_server.png)

Das Gleiche habe ich danach noch mit den Namen der Hosts (A-records) gemacht.

![nslookup_FQDN](./images/nslookup_FQDM.png)

Zwischen dem Client und dem Server hat jeder nslookup Test funktioniert. Sowohl auf die beiden IP-Adressen, als auch auf die Namen.

Client: client.sota.com
        192.168.100.53

Server: server.sota.com
        192.168.100.2



### Forwarding

Ich habe in dieser praktischen Aufgabe einen DNS-Server in einem Lokalen Netzwerk konfiguriert. Allerdings kann der Client, weil ich bis jetzt nur 2 Hosts (A-Records) hinzugefügt habe, nur die IP-Adressen/FQDM abfragen, die ich manuell im DNS-Server hinterlegt habe.

##### Authoritativ
Wenn ein DNS-Server diese Info bei sich (so zu sagen) hostet, dan ist er ein so genanner "authoritativer" DNS-Server. Ein "authoritativer" DNS-Server ist selber für die Anfragen verantwortlich, die er vom Client bekommt.

##### Recursive
Wenn der Server diese Infos allerdings nicht bei sich hostet, muss er die Anfrage vom Client an einen anderen (hinterlegten) DNS-Server weitergeben. Einen solchen DNS-Server nennt man auch "recursive".

Den Prozess des Weitergebens der Anfrage des Clients nennt man auch "Forwarding"

Einen Forwarding Server fügt man in dieser Aufgabe folgendermassen hinzu: 

Rechtsklick auf den DNS-Server (bei mir heist das Feld WIN-CM32R3CMPRV) und dann auf "Properties".

![DNS_Properties](./images/DNS_Properties.png)

Dann muss man auf "Forwarders" klicken und dort die IP-Adresse des DNS eingeben, anden die Anfrage des Clients weitergeleitet werden soll. Ich habe hier den Google DNS verwendet mit der IP "8.8.8.8". Wenn man diese Adresse eingibt, sollte automatisch der DNS-Server von google mit der eingetragenen IP-Adresse und dem FQDN "dns.google" angezeigt werden. Sobald man auf "OK" dürckt ist das FOrwarding abgeschlossen.

![google_forwarding](./images/google_forwarding.png)

Da ich den DNS-Server von google als Forward angegeben habe, kann ich nun die funktion testen indem ich jeden beliebigen FQDN von einer auf google verfügberen Internetseite eingebe. Ich habe hier als Beispiel "brack.ch" gewählt.

![nslookup_google](./images/nslookup_google.png)

Der Test hat funktioniert und somit war der Auftrag erfolgreich.